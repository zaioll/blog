<?php

declare(strict_types=1);

namespace Zaioll\Blog\Shared\Domain\Model\Post;

use Zaioll\Blog\Domain\Model\Post\Post;
use Zaioll\Shared\Infrastructure\Helper\ArrayHelper;

final class PostResultSet
{
    private $posts;

    private $total;

    /**
     * @param Post[] $post
     */
    public function __construct(array $posts, int $total, array $fields)
    {
        $this->posts = [];
        $this->total = $total;
        if ($total > 0) {
            foreach ($posts as $post) {
                if (! $post instanceof Post) {
                    continue;
                }
                /** @var Post $post */
                $this->posts[] = $post->getState();
            }
        }
    }

    /**
     * @return mixed[]
     */
    public function getPosts(): array
    {
        return $this->posts;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param Post|array $post
     * @return self
     */
    public function add($post): self
    {
        if ($post instanceof Post) {
            $this->posts[] = $post->getState();
            $this->total += 1;
        }
        if (! is_array($post)) {
            return $this;
        }

        if (! ArrayHelper::keyExists('id', $post)) {
            return $this;
        }
        $this->posts[] = $post;
        $this->total += 1;

        return $this;
    }
}
