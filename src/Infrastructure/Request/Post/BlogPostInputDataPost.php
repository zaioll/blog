<?php

declare(strict_types=1);

namespace Zaioll\Blog\Infrastructure\Request\Post;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Zaioll\Blog\Application\BlogPost\Post\PostBlogPostCommand;

final class BlogPostInputDataPost extends AbstractType
{
    /**
     * @Assert\NotBlank
     */
    private $title;

    /**
     * @Assert\NotBlank
     */
    private $content;

    /**
     * @Assert\NotBlank
     */
    private $authorName;

    public function getTitle(): string
    {
        return (string) ($this->title ?? '');
    }

    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): string
    {
        return (string) ($this->content ?? '');
    }

    public function setContent(string $content)
    {
        $this->content = $content;

        return $this;
    }

    public function setAuthorName(string $authorName)
    {
        $this->authorName = $authorName;

        return $this;
    }

    public function getAuthorName(): string
    {
        return (string) ($this->authorName ?? '');
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $form = $builder
            ->add('title', TextType::class, ['required' => true, 'label' => 'Title'])
            ->add('content', TextType::class, ['required' => true, 'label' => 'Content'])
            ->add('author_name', TextType::class, ['required' => true, 'label' => 'Author Name'])
            ->add('submit', SubmitType::class, [])
            ->getForm();

        return $form;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => static::class,
        ]); 
    }

    /**
     * Get's command DTO object.
     *
     * @return PostBlogPostCommand
     */
    public function getCommand(): PostBlogPostCommand
    {
        return new PostBlogPostCommand(
            $this->getTitle(),
            $this->getContent(),
            $this->getAuthorName()
        );
    }
}
