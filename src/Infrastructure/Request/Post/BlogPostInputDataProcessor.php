<?php

declare(strict_types=1);

namespace Zaioll\Blog\Infrastructure\Request\Post;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Zaioll\Blog\Infrastructure\Request\Post\BlogPostInputDataPost;
use Zaioll\Shared\Infrastructure\Component\Symfony\InputRequest\SymfonyInputDataProcessor;

final class BlogPostInputDataProcessor extends SymfonyInputDataProcessor
{
    /**
     * @inheritDoc
     */
    protected function extractAndValidateData(Request $request): FormInterface
    {
        $builder = $this->formFactory->createBuilder(BlogPostInputDataPost::class);
        $form = $builder->getForm();

        $form->handleRequest($request);

        return $form;
    }
}
