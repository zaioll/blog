<?php

declare(strict_types=1);

namespace Zaioll\Blog\Infrastructure\Request\Put;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Zaioll\Shared\Infrastructure\Helper\StringHelper;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Zaioll\Blog\Infrastructure\Request\Put\BlogPostPutInputData;
use Zaioll\Shared\Infrastructure\Component\Symfony\InputRequest\SymfonyInputDataProcessor;

final class BlogPostPutInputDataProcessor extends SymfonyInputDataProcessor
{
    /**
     * @inheritDoc
     */
    protected function extractAndValidateData(Request $request): FormInterface
    {
        $builder = $this->formFactory->createBuilder(BlogPostPutInputData::class);
        $form = $builder->getForm();

        if (! $form->isSubmitted()) {
            $form->add('id', HiddenType::class, ['required' => true, 'data' => StringHelper::explode($request->getRequestUri(), '/')[3] ?? '']);
        }
        $form->handleRequest($request);

        return $form;
    }
}
