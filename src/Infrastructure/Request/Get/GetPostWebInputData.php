<?php

declare(strict_types=1);

namespace Zaioll\Blog\Infrastructure\Request\Get;

use Symfony\Component\Validator\Validation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Zaioll\Shared\Infrastructure\Helper\StringHelper;
use Symfony\Component\Validator\Constraints as Assert;
use Zaioll\Blog\Application\BlogPost\Get\Single\GetPostQuery;
use Zaioll\Shared\Infrastructure\InputRequest\InputDataAbstract;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Zaioll\Shared\Infrastructure\InputRequest\SymfonyBadRequestHttpException;

final class GetPostWebInputData extends InputDataAbstract
{
    private $id;

    public function getId(): string
    {
        return (string) ($this->id ?? '');
    }

    protected function extractAndValidateData(Request $request): ConstraintViolationListInterface
    {
        $parameters = [];
        $parameters['id'] = StringHelper::explode($request->getRequestUri(), '/')[2] ?? '';

        $inputConstraints = new Assert\Collection([
            'id' => [new Assert\NotBlank(['message' => 'Post ID must be not blank!']), new Assert\Uuid(['message' => "Post ID '{$parameters['id']}' is invalid!"])],
        ]);

        $violations = Validation::createValidator()->validate($parameters, $inputConstraints);

        if ($violations->count() === 0) {
            $this->id = $parameters['id'];
        }

        return $violations;
    }

    /**
     * @return GetPostQuery
     */
    public function getQuery(): GetPostQuery
    {
        return new GetPostQuery($this->id);
    }

    /**
     * @inheritDoc
     */
    protected function handlesErrors()
    {
        throw new SymfonyBadRequestHttpException($this->errors, Response::HTTP_BAD_REQUEST);
    }
}
