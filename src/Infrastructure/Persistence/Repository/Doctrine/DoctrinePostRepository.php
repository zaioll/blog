<?php

declare(strict_types=1);

namespace Zaioll\Blog\Infrastructure\Persistence\Repository\Doctrine;

use Exception;
use Zaioll\Blog\Domain\Model\Post\Post;
use Doctrine\Persistence\ManagerRegistry;
use Zaioll\Blog\Domain\Model\Post\PostRepository;
use Zaioll\Blog\Domain\Model\Post\PostPublishFailed;
use Zaioll\Shared\Infrastructure\Helper\ArrayHelper;
use Zaioll\Blog\Shared\Domain\Model\Post\PostResultSet;
use Zaioll\Shared\Infrastructure\Component\Uid\UuidAdapter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Ramsey\Uuid\UuidInterface;

final class DoctrinePostRepository extends ServiceEntityRepository implements PostRepository
{
    public function __construct(
        ManagerRegistry $registry
    ) {
        parent::__construct($registry, Post::class);
    }

    /**
     * @throws PostPublishFailed
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function post(Post $post): void
    {
        try {
            $this->_em->persist($post);
            $this->_em->flush();
        } catch (Exception $ex) {
            throw new PostPublishFailed('', $ex->getCode(), $ex);
        }
    }

    public function getAll(): PostResultSet
    {
        $allPosts = [];
        $result = $this->findAll();
        $qty = ArrayHelper::count($result);
        if ($qty === 0) {
            return new PostResultSet([], 0, []);
        }

        $allPosts = new PostResultSet([], 0, []);
        foreach ($result as $post) {
            $allPosts->add($post);
        }

        return $allPosts;
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete(Post $post): void
    {
        $id = $post->getId()->toString();

        $this->_em->remove($this->_em->merge($post));
        $this->_em->flush();
    }

    /**
     * @param  UuidInterface|string $name
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \JsonException
     */
    public function getById($id): ?Post
    {
        if (is_string($id)) {
            $id = UuidAdapter::fromString($id);
        }
        if (! $id instanceof UuidInterface) {
            return null;
        }

        $post = $this->_em->find(Post::class, $id);

        if (! empty($post)) {
            return $post;
        }

        return null;
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     * @throws \JsonException
     */
    public function put(Post $post): void
    {
        $this->_em->persist($this->_em->merge($post));
        $this->_em->flush();
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     * @throws \JsonException
     */
    public function patch(Post $post): void
    {
        $this->_em->persist($post);
        $this->_em->flush();
    }
}
