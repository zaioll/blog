<?php

declare(strict_types=1);

namespace Zaioll\Blog\Infrastructure\Persistence\Repository\InMemory;

use Ramsey\Uuid\UuidInterface;
use Zaioll\Blog\Domain\Model\Post\Post;
use Zaioll\Blog\Domain\Model\Post\PostRepository;
use Zaioll\Shared\Infrastructure\Helper\ArrayHelper;
use Zaioll\Blog\Shared\Domain\Model\Post\PostResultSet;
use Zaioll\Shared\Infrastructure\Component\Uid\UuidAdapter;

class InMemoryPostRepository implements PostRepository
{
    /**
     * @var array
     */
    private $records;

    public function __construct(array $posts = [])
    {
        $this->records = [];
        $qty = ArrayHelper::count($posts);
        if ($qty > 0) {
            $qty = 0;
            foreach ($posts as $post) {
                if (! $post instanceof Post) {
                    continue;
                }
                $this->push($post);
                $qty += 1;
            }
        }
        $this->records['qty'] = $qty;
    }

    private function push(Post $item): string
    {
        $this->records["{$item->getId()->toString()}"] = $item;
        $this->records['qty'] += 1;

        return $item->getId()->toString();
    }

    /**
     * @inheritDoc
     */
    public function getAll(): PostResultSet
    {
        return new PostResultSet($this->records ?? [], $this->records['qty'] ?? 0, []);
    }

    /**
     * @inheritDoc
     */
    public function post(Post $post): void
    {
        $this->push($post);
    }

    /**
     * @inheritDoc
     */
    public function delete(Post $post): void
    {
        $result = ArrayHelper::remove($this->records, "{$post->getId()->toString()}");
        if ($result !== null && $this->records['qty'] > 0) {
            $this->records['qty'] -= 1;
        }
    }

    /**
     * @inheritDoc
     */
    public function getById($id): ?Post
    {
        if (is_string($id)) {
            $id = UuidAdapter::fromString($id);
        }
        if (! $id instanceof UuidInterface) {
            return null;
        }
        return ArrayHelper::getValue($this->records, "{$id->toString()}}");
    }

    /**
     * @inheritDoc
     */
    public function put(Post $post): void
    {
        $this->push($post);
    }

    /**
     * @inheritDoc
     */
    public function patch(Post $post): void
    {
        $this->push($post);
    }
}
