<?php

declare(strict_types=1);

namespace Zaioll\Blog\Infrastructure\Persistence\Repository\Redis;

use Ramsey\Uuid\UuidInterface;
use Zaioll\Blog\Domain\Model\Post\Post;
use Zaioll\Blog\Domain\Model\Post\PostRepository;
use Zaioll\Shared\Infrastructure\Helper\ArrayHelper;
use Zaioll\Shared\Infrastructure\Helper\StringHelper;
use Zaioll\Blog\Shared\Domain\Model\Post\PostResultSet;
use Zaioll\Shared\Infrastructure\Component\Uid\UuidAdapter;
use Zaioll\Shared\Infrastructure\Component\Redis\RedisClientAdapter;

class RedisPostRepository implements PostRepository
{
    /**
     * @var RedisClientAdapter
     */
    private $records;

    private $qty;

    public function __construct(RedisClientAdapter $redis, array $posts = [])
    {
        $this->records = $redis;

        $qty = ArrayHelper::count($posts);
        if ($qty > 0) {
            $qty = 0;
            foreach ($posts as $post) {
                if (! $post instanceof Post) {
                    continue;
                }
                $this->push($post);
                $qty += 1;
            }
        }
        $this->qty = $qty;
    }

    private function push(Post $item): string
    {
        $post = ArrayHelper::toJson($item->getState(), JSON_HEX_APOS); 
        if ($post !== false) {
            $this->records->set("records:post:{$item->getId()->toString()}", $post);
            $this->qty += 1;
        }

        return $item->getId()->toString();
    }

    /**
     * @inheritDoc
     */
    public function getAll(): PostResultSet
    {
        $keys = $this->records->scan('records:post:*');
        if ($keys === false) {
            return new PostResultSet([], $this->qty, []);
        }
        $result = new PostResultSet([], 0, []);
        foreach ($keys as $key) {
            $post = $this->records->get("{$key}");
            if ($post === false) {
                continue;
            }
            $result->add(StringHelper::jsonDecodeToAssociative($post));
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function post(Post $post): void
    {
        $this->push($post);
    }

    /**
     * @inheritDoc
     */
    public function delete(Post $post): void
    {
        $this->records->del("records:post:{$post->getId()->toString()}");
    }

    /**
     * @inheritDoc
     */
    public function getById($id): ?Post
    {
        if (is_string($id)) {
            $id = UuidAdapter::fromString($id);
        }
        if (! $id instanceof UuidInterface) {
            return null;
        }
        $post = $this->records->get("records:post:{$id->toString()}");
        if ($post === false) {
            return null;
        }
        return Post::fromState(StringHelper::jsonDecodeToAssociative($post));
    }

    /**
     * @inheritDoc
     */
    public function put(Post $post): void
    {
        $this->push($post);
    }

    /**
     * @inheritDoc
     */
    public function patch(Post $post): void
    {
        $this->push($post);
    }
}
