<?php

declare(strict_types=1);

namespace Zaioll\Blog\Infrastructure\Persistence\Repository\Redis;

use Zaioll\Shared\Infrastructure\Component\Redis\RedisSimpleCache;

final class RedisPostCache extends RedisSimpleCache
{
    protected const PREFIX_KEY = 'cache.post';
}
