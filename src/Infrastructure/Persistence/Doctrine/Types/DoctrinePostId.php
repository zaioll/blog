<?php

declare(strict_types=1);

namespace Zaioll\Blog\Infrastructure\Persistence\Doctrine\Types;

use Ramsey\Uuid\Doctrine\UuidType;

final class DoctrinePostId extends UuidType
{
    const NAME = 'PostId';
}
