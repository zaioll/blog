<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Get;

use Zaioll\Shared\Domain\Bus\Response;
use Zaioll\Blog\Shared\Domain\Model\Post\PostResultSet;

final class GetPostsResponse implements Response
{
    private $posts;

    public function __construct(PostResultSet $posts)
    {
        $this->posts = $posts;        
    }

    public function getTotal(): int
    {
        return $this->posts->getTotal();
    }

    public function getPosts(): array
    {
        return $this->posts->getPosts();
    }
}
