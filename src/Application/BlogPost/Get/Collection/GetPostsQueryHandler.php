<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Get\Collection;

use Zaioll\Shared\Domain\Bus\Query\QueryHandler;
use Zaioll\Shared\Domain\Bus\MessageBusEnvelopeInstantiator;
use Zaioll\Blog\Application\BlogPost\Get\Collection\GetPostsQuery;
use Zaioll\Shared\Application\Service\ApplicationServiceInterface;

class GetPostsQueryHandler implements QueryHandler
{
    private $searcher;

    private $envelopeInstantiator;

    public function __construct(ApplicationServiceInterface $allPostsSearcher, MessageBusEnvelopeInstantiator $envelopeInstantiator)
    {
        $this->searcher             = $allPostsSearcher;
        $this->envelopeInstantiator = $envelopeInstantiator;
    }

    public function __invoke(GetPostsQuery $query)
    {
        return $this->envelopeInstantiator->createEnvelope($this->searcher->execute($query));
    }
}
