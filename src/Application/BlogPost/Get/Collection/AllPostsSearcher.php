<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Get\Collection;

use Zaioll\Shared\Domain\Bus\Request;
use Zaioll\Blog\Domain\Model\Post\PostRepository;
use Zaioll\Blog\Application\BlogPost\Get\GetPostsResponse;
use Zaioll\Blog\Application\BlogPost\Get\Collection\GetPostsQuery;
use Zaioll\Shared\Application\Service\ApplicationServiceInterface;

class AllPostsSearcher implements ApplicationServiceInterface
{
    private $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository; 
    }

    /**
     * @inheritDoc
     */
    public static function isTransactional(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function execute(Request $request)
    {
        /** @var GetPostsQuery */
        return new GetPostsResponse($this->postRepository->getAll());
    }
}
