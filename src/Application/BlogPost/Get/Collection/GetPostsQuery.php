<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Get\Collection;

use Zaioll\Shared\Domain\Bus\Query\InternalQuery;

class GetPostsQuery implements InternalQuery
{
}
