<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Get\ByCriteria;

use Zaioll\Shared\Domain\Bus\Query\InternalQuery;

class GetPostsByCriteriaQuery implements InternalQuery
{
    private $filters;

    private $orderBy;

    private $order;

    private $offset;

    private $limit;

    public function __construct(
        array $filters,
        string $orderBy,
        string $order,
        string $offset,
        int $limit
    ) {
        $this->filters  = $filters;
        $this->orderBy  = $orderBy;
        $this->order    = $order;
        $this->offset   = $offset;
        $this->limit    = $limit;
    }

    /**
     * @return string
     */
    public function offset(): string
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function limit(): int
    {
        return $this->limit;
    }

    public function filters(): array
    {
        return $this->filters;
    }

    /**
     * @return string
     */
    public function orderBy(): string
    {
        return $this->orderBy;
    }

    /**
     * @return string
     */
    public function order(): string
    {
        return $this->order;
    }
}
