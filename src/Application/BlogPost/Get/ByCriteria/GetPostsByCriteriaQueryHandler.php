<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Get\ByCriteria;

use Zaioll\Shared\Domain\Bus\Query\QueryHandler;
use Zaioll\Shared\Domain\Bus\MessageBusEnvelopeInstantiator;
use Zaioll\Shared\Application\Service\ApplicationServiceInterface;
use Zaioll\Blog\Application\BlogPost\Get\ByCriteria\GetPostsByCriteriaQuery;

class GetPostsByCriteriaQueryHandler implements QueryHandler
{
    private $searcher;

    private $envelopeInstantiator;

    public function __construct(ApplicationServiceInterface $postSearcher, MessageBusEnvelopeInstantiator $envelopeInstantiator)
    {
        $this->searcher             = $postSearcher;        
        $this->envelopeInstantiator = $envelopeInstantiator;
    }

    public function __invoke(GetPostsByCriteriaQuery $query)
    {
        return $this->envelopeInstantiator->createEnvelope($this->searcher->execute($query));

    }
}
