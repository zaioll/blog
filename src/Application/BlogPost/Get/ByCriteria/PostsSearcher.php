<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Get\ByCriteria;

use Zaioll\Shared\Domain\Bus\Request;
use Zaioll\Shared\Domain\Criteria\Order;
use Zaioll\Shared\Domain\Criteria\Filters;
use Zaioll\Shared\Domain\Criteria\Criteria;
use Zaioll\Blog\Domain\Model\Post\PostNotFound;
use Zaioll\Blog\Domain\Model\Post\PostRepository;
use Zaioll\Blog\Shared\Domain\Model\Post\PostResultSet;
use Zaioll\Blog\Application\BlogPost\Get\GetPostsResponse;
use Zaioll\Shared\Application\Service\ApplicationServiceInterface;
use Zaioll\Blog\Application\BlogPost\Get\ByCriteria\GetPostByCriteriaQuery;
use Zaioll\Shared\Infrastructure\Component\Doctrine\DoctrineCriteriaConverter;

class PostsSearcher implements ApplicationServiceInterface
{
    private $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository; 
    }

    /**
     * @inheritDoc
     */
    public static function isTransactional(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function execute(Request $request)
    {
        /** @var GetPostByCriteriaQuery $request */
        $filters = Filters::fromValues($request->filters());
        $order   = Order::fromValues($request->orderBy(), $request->order());

        $post = $this->search(new Criteria($filters, $order, $request->limit(), $request->offset()));
        if ($post === null) {
            throw new PostNotFound('');
        }
        $resultSet = new PostResultSet($post->getState(), 1, []);
        return new GetPostsResponse($resultSet);
    }

    public function search(Criteria $criteria): ?Post
    {
        $criteria = DoctrineCriteriaConverter::convert($criteria);
        $result = $this->postRepository->matching($criteria);
        if (! $result) {
            return null;
        }

        return $result;
    }
}
