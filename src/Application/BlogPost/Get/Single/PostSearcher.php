<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Get\Single;

use Zaioll\Shared\Domain\Bus\Request;
use Zaioll\Blog\Shared\Domain\Model\Post\PostResultSet;
use Zaioll\Blog\Domain\Model\Post\PostRepository;
use Zaioll\Blog\Application\BlogPost\Get\GetPostsResponse;
use Zaioll\Blog\Application\BlogPost\Get\Single\GetPostQuery;
use Zaioll\Blog\Domain\Model\Post\PostNotFound;
use Zaioll\Shared\Application\Service\ApplicationServiceInterface;
use Zaioll\Shared\Infrastructure\Component\Uid\UuidAdapter;

class PostSearcher implements ApplicationServiceInterface
{
    private $postRepository;

    public function __construct(PostRepository $readPostRepository)
    {
        $this->postRepository = $readPostRepository; 
    }

    /**
     * @inheritDoc
     */
    public static function isTransactional(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function execute(Request $request)
    {
        /** @var GetPostQuery $request */
        $postId = UuidAdapter::fromString($request->getId());
        $post = $this->postRepository->getById($postId);
        if ($post === null) {
            throw new PostNotFound("Post id: '{$request->getId()}' not found!");
        }

        $resultSet = new PostResultSet([$post], 1, []);

        return new GetPostsResponse($resultSet);
    }
}
