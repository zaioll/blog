<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Get\Single;

use Zaioll\Shared\Domain\Bus\Query\InternalQuery;

class GetPostQuery implements InternalQuery
{
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }
}
