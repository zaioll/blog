<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Post;

use Zaioll\Shared\Domain\Bus\Response;

final class CreatePostResponse implements Response
{
    private $isSuccess;

    public function __construct(bool $isSuccess)
    {
        $this->isSuccess = $isSuccess;        
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }
}
