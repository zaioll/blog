<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Post;

use Zaioll\Shared\Domain\Bus\Command\SyncCommand;

class PostBlogPostCommand implements SyncCommand
{
    private $authorName;

    private $title;

    private $content;

    public function __construct(string $title, string $content, string $authorName)
    {
        $this->authorName   = $authorName;
        $this->title        = $title;
        $this->content      = $content;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getAuthorName(): string
    {
        return $this->authorName;
    }
}
