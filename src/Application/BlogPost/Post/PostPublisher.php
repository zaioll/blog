<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Post;

use Zaioll\Shared\Domain\Bus\Request;
use Zaioll\Blog\Domain\Model\Post\Post;
use Zaioll\Blog\Domain\Model\Post\PostRepository;
use Zaioll\Shared\Domain\Bus\Event\EventPublisher;
use Zaioll\Blog\Application\BlogPost\Post\PostBlogPostCommand;
use Zaioll\Shared\Application\Service\ApplicationServiceInterface;

class PostPublisher implements ApplicationServiceInterface
{
    private $postRepository;

    private $eventPublisher;

    public function __construct(
        PostRepository $postRepository,
        EventPublisher $eventPublisher
    ) {
        $this->postRepository = $postRepository;
        $this->eventPublisher = $eventPublisher;
    }

    /**
     * @inheritDoc
     */
    public static function isTransactional(): bool
    {
        return true;
    }

    public function execute(Request $request)
    {
        /** @var PostBlogPostCommand $request */
        $blogPost = Post::create(
            $request->getTitle(),
            $request->getContent(),
            $request->getAuthorName()
        );
        $this->postRepository->post($blogPost);
        //$this->eventPublisher->publish(...$blogPost->dropEvents());
        return new CreatePostResponse(true);
    }
}
