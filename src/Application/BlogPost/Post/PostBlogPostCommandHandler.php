<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Post;

use Zaioll\Shared\Domain\Bus\Command\CommandHandler;
use Zaioll\Shared\Domain\Bus\MessageBusEnvelopeInstantiator;
use Zaioll\Blog\Application\BlogPost\Post\PostBlogPostCommand;
use Zaioll\Shared\Application\Service\ApplicationServiceInterface;

class PostBlogPostCommandHandler implements CommandHandler
{
    private $postPublisher;

    private $envelopeInstantiator;

    public function __construct(
        ApplicationServiceInterface $postPublisher,
        MessageBusEnvelopeInstantiator $envelopeInstantiator
    ) {
        $this->postPublisher        = $postPublisher;        
        $this->envelopeInstantiator = $envelopeInstantiator;
    }

    public function __invoke(PostBlogPostCommand $command)
    {
        return $this->envelopeInstantiator->createEnvelope($this->postPublisher->execute($command));
    }
}
