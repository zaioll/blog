<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Put;

use Zaioll\Shared\Domain\Bus\Request;
use Zaioll\Blog\Domain\Model\Post\PostNotFound;
use Zaioll\Blog\Domain\Model\Post\PostRepository;
use Zaioll\Shared\Domain\Bus\Event\EventPublisher;
use Zaioll\Blog\Application\BlogPost\Put\BlogPostPutCommand;
use Zaioll\Blog\Application\BlogPost\Put\UpdatePostResponse;
use Zaioll\Shared\Application\Service\ApplicationServiceInterface;
use Zaioll\Shared\Infrastructure\Component\Uid\UuidAdapter;

class PostUpdater implements ApplicationServiceInterface
{
    private $postRepository;

    private $eventPublisher;

    public function __construct(
        PostRepository $postRepository,
        EventPublisher $eventPublisher
    ) {
        $this->postRepository = $postRepository;
        $this->eventPublisher = $eventPublisher;
    }

    /**
     * @inheritDoc
     */
    public static function isTransactional(): bool
    {
        return true;
    }

    public function execute(Request $request)
    {
        /** @var BlogPostPutCommand $request */
        $post = $this->postRepository->getById(UuidAdapter::fromString($request->getId()));
        if ($post === null) {
            throw new PostNotFound("Post id: '{$request->getId()}' not found!");
        }
        $post
            ->setAuthorName($request->getAuthorName())
            ->setContent($request->getContent())
            ->setTitle($request->getTitle())
        ;
        $this->postRepository->put($post);

        //$this->eventPublisher->publish(...$blogPost->dropEvents());
        return new UpdatePostResponse(true);
    }
}
