<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Put;

use Zaioll\Shared\Domain\Bus\Command\SyncCommand;

class BlogPostPutCommand implements SyncCommand
{
    private $authorName;

    private $title;

    private $content;

    private $id;

    public function __construct(string $id, string $title, string $content, string $authorName)
    {
        $this->id           = $id;
        $this->authorName   = $authorName;
        $this->title        = $title;
        $this->content      = $content;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getAuthorName(): string
    {
        return $this->authorName;
    }
}
