<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Put;

use Zaioll\Shared\Domain\Bus\Command\CommandHandler;
use Zaioll\Blog\Application\BlogPost\Put\BlogPostPutCommand;
use Zaioll\Shared\Domain\Bus\MessageBusEnvelopeInstantiator;
use Zaioll\Shared\Application\Service\ApplicationServiceInterface;

class BlogPostPutCommandHandler implements CommandHandler
{
    private $postPublisher;

    private $envelopeInstantiator;

    public function __construct(
        ApplicationServiceInterface $postUpdater,
        MessageBusEnvelopeInstantiator $envelopeInstantiator
    ) {
        $this->postPublisher        = $postUpdater;        
        $this->envelopeInstantiator = $envelopeInstantiator;
    }

    public function __invoke(BlogPostPutCommand $command)
    {
        return $this->envelopeInstantiator->createEnvelope($this->postPublisher->execute($command));
    }
}
