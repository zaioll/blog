<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Delete;

use Zaioll\Shared\Domain\Bus\Command\CommandHandler;
use Zaioll\Shared\Domain\Bus\MessageBusEnvelopeInstantiator;
use Zaioll\Blog\Application\BlogPost\Delete\DeletePostCommand;
use Zaioll\Shared\Application\Service\ApplicationServiceInterface;

class DeletePostCommandHandler implements CommandHandler
{
    private $deleter;

    private $envelopeInstantiator;

    public function __construct(ApplicationServiceInterface $postDeleter, MessageBusEnvelopeInstantiator $envelopeInstantiator)
    {
        $this->deleter = $postDeleter;        
        $this->envelopeInstantiator = $envelopeInstantiator;
    }

    public function __invoke(DeletePostCommand $command)
    {
        return $this->envelopeInstantiator->createEnvelope($this->deleter->execute($command));

    }
}
