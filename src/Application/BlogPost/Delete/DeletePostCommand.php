<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Delete;

use Zaioll\Shared\Domain\Bus\Command\InternalCommand;

class DeletePostCommand implements InternalCommand
{
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }
}
