<?php

declare(strict_types=1);

namespace Zaioll\Blog\Application\BlogPost\Delete;

use Zaioll\Shared\Domain\Bus\Request;
use Zaioll\Blog\Domain\Model\Post\PostNotFound;
use Zaioll\Blog\Domain\Model\Post\PostRepository;
use Zaioll\Blog\Application\BlogPost\Delete\DeletePostCommand;
use Zaioll\Blog\Application\BlogPost\Delete\DeletePostResponse;
use Zaioll\Shared\Application\Service\ApplicationServiceInterface;
use Zaioll\Shared\Infrastructure\Component\Uid\UuidAdapter;

class PostDeleter implements ApplicationServiceInterface
{
    private $postRepository;

    public function __construct(PostRepository $readPostRepository)
    {
        $this->postRepository = $readPostRepository; 
    }

    /**
     * @inheritDoc
     */
    public static function isTransactional(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function execute(Request $request)
    {
        /** @var DeletePostCommand $request */
        $postId = UuidAdapter::fromString($request->getId());
        $post = $this->postRepository->getById($postId);
        if ($post === null) {
            throw new PostNotFound("Post id: '{$request->getId()}' not found!");
        }

        $this->postRepository->delete($post);

        return new DeletePostResponse(true);
    }
}
