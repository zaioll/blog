<?php

declare(strict_types=1);

namespace Zaioll\Blog\Domain\Model\Post;

use DateTimeImmutable;
use Zaioll\Shared\Domain\Model\Entity;
use Zaioll\Shared\Infrastructure\Helper\ArrayHelper;
use Zaioll\Shared\Infrastructure\Helper\StringHelper;
use Zaioll\Shared\Infrastructure\Helper\DateTimeHelper;
use Ramsey\Uuid\Uuid;

class Post implements Entity
{
    private $id;

    private $authorName;

    private $content;

    private $title;

    private $createdAt;

    private function __construct()
    {
        $this->id           = Uuid::uuid4(); 
        $this->createdAt    = DateTimeHelper::createDateTimeImmutableUtcTimeZone('now'); 
    }

    /**
     * @return \Ramsey\Uuid\Lazy\LazyUuidFromString
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return (string) $this->title;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatingDate(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return (string) $this->content;
    }

    /**
     * @return string
     */
    public function getAuthorName(): string
    {
        return (string) $this->authorName;
    }

    public static function create(
        string $title,
        string $content,
        string $authorName
    ): self {
        $post               = new static();
        $post->title        = $title;
        $post->content      = $content;
        $post->authorName   = $authorName;

        return $post;
    }

    /**
     * @inheritDoc
     */
    public function getState(): array
    {
        $state = [
            'title'         => $this->getTitle(), 
            'created_at'    => $this->getCreatingDate()->format(DATE_ISO8601), 
            'content'       => $this->getContent(),
            'author_name'   => $this->getAuthorName(),
        ];

        if ($this->id !== null) {
            $state['id'] = $this->id->toString();
        }

        return $state;
    }

    /**
     * @param string $content
     * @return self
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @param string $title
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param string $authorName
     * @return self
     */
    public function setAuthorName(string $authorName): self
    {
        $this->authorName = $authorName;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public static function fromState(array $state): Entity
    {
        $object = new static();
        if (ArrayHelper::count($state) === 0) {
            return $object;
        }
        foreach ($state as $property => $value) {
            $prop = StringHelper::toCamelCase($property);
            if (! property_exists($object, $prop)) {
                continue;
            }
            if ($prop === 'id' && is_string($value)) {
                $value = Uuid::fromString($value);
            }
            if ($prop === 'createdAt') {
                $value = DateTimeHelper::createDateTimeImmutableUtcTimeZone($value);
            }
            $object->{$prop} = $value;
        }

        return $object;
    }
}
