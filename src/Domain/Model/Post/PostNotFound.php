<?php

declare(strict_types=1);

namespace Zaioll\Blog\Domain\Model\Post;

use Zaioll\Shared\Domain\Model\DomainException;

class PostNotFound extends DomainException 
{
}
