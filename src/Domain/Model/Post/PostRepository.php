<?php

declare(strict_types=1);

namespace Zaioll\Blog\Domain\Model\Post;

use Ramsey\Uuid\UuidInterface;
use Zaioll\Blog\Domain\Model\Post\Post;
use Zaioll\Blog\Shared\Domain\Model\Post\PostResultSet;

interface PostRepository
{
    public function getAll(): PostResultSet;

    public function post(Post $post): void;

    public function delete(Post $post): void;

    /**
     * @param UuidInterface|string $id
     * @return Post|null
     */
    public function getById($id): ?Post;

    public function put(Post $post): void;

    public function patch(Post $post): void;
}
