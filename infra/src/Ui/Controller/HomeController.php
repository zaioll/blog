<?php

declare(strict_types=1);

namespace Zaioll\Blog\Infrastructure\Ui\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="homepage")
     */
    public function __invoke()
    {
        return $this->redirectToRoute('post_list');
    }
}
