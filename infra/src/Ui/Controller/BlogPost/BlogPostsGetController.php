<?php

declare(strict_types=1);

namespace Zaioll\Blog\Infrastructure\Ui\Controller\BlogPost;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Messenger\MessageBusInterface;
use Zaioll\Blog\Application\Post\Get\Collection\GetPostsResponse;
use Zaioll\Blog\Application\BlogPost\Get\Collection\GetPostsQuery;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogPostsGetController extends AbstractController
{
    private $queryBus;

    public function __construct(MessageBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @Route("/posts", methods={"GET"}, name="post_list")
     */
    public function __invoke()
    {
        /** @var HandledStamp $handledStamp */
        $handledStamp = $this->queryBus
            ->dispatch(new GetPostsQuery())
            ->last(HandledStamp::class) 
        ;
        /** @var GetPostsResponse $data */
        $data = $handledStamp->getResult()->getMessage();

        return $this->render('post/list.html.twig', ['posts' => $data->getPosts()]);
    }
}
