<?php

declare(strict_types=1);

namespace Zaioll\Blog\Infrastructure\Ui\Controller\BlogPost;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Zaioll\Blog\Infrastructure\Request\Post\BlogPostInputDataProcessor;

class BlogPostPostController extends AbstractController
{
    private $commandBus;

    public function __construct(MessageBusInterface $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @Route("/post/create", methods={"POST", "GET"}, name="new_post")
     */
    public function __invoke(BlogPostInputDataProcessor $processor)
    {
        $form = $processor->getForm();
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var HandledStamp $handledStamp */
            $handledStamp = $this->commandBus
                ->dispatch($form->getData()->getCommand())
                ->last(HandledStamp::class) 
            ;
            $data = $handledStamp->getResult()->getMessage();
            if ($data->isSuccess()) {
                return $this->redirectToRoute('homepage');
            }
        }
        return $this->render('post/new.html.twig', ['form' => $form->createView()]);
    }
}
