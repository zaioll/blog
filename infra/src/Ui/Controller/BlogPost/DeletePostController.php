<?php

declare(strict_types=1);

namespace Zaioll\Blog\Infrastructure\Ui\Controller\BlogPost;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Messenger\MessageBusInterface;
use Zaioll\Blog\Application\Post\Get\Collection\GetPostsResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Zaioll\Blog\Infrastructure\Request\Delete\DeletePostWebInputData;

class DeletePostController extends AbstractController
{
    private $commandBus;

    public function __construct(MessageBusInterface $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @Route("/post/delete/{id}", methods={"POST", "GET"}, name="post_del")
     */
    public function __invoke(DeletePostWebInputData $inputData)
    {
        /** @var HandledStamp $handledStamp */
        $handledStamp = $this->commandBus
            ->dispatch($inputData->getCommand())
            ->last(HandledStamp::class) 
        ;
        /** @var GetPostsResponse $data */
        $data = $handledStamp->getResult()->getMessage();
        return $this->redirectToRoute('homepage');
    }
}
