<?php

declare(strict_types=1);

namespace Zaioll\Blog\Infrastructure\Ui\Controller\BlogPost;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Messenger\MessageBusInterface;
use Zaioll\Blog\Infrastructure\Request\Get\GetPostWebInputData;
use Zaioll\Blog\Application\Post\Get\Collection\GetPostsResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GetPostController extends AbstractController
{
    private $queryBus;

    public function __construct(MessageBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @Route("/post/{id}", methods={"GET"}, name="post_get")
     */
    public function __invoke(GetPostWebInputData $inputData)
    {
        /** @var HandledStamp $handledStamp */
        $handledStamp = $this->queryBus
            ->dispatch($inputData->getQuery())
            ->last(HandledStamp::class) 
        ;
        /** @var GetPostsResponse $data */
        $data = $handledStamp->getResult()->getMessage();

        return $this->render('post/view.html.twig', ['posts' => $data->getPosts()]);
    }
}
